import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/views/administration/admin.dart';
import 'package:up_pro/views/common/accounts/page_scanner_login.dart';
import 'package:up_pro/views/common/main_page.dart';
import 'package:up_pro/views/home_page/home_page.dart';
import 'package:up_pro/views/parametre/settings.dart';
import 'package:up_pro/views/profil/profil.dart';
import '../views/list_items/list_items.dart';
import '../views/text/steppers.dart';
import '../views/historique/historique.dart';
import '../views/common/accounts/login_page.dart';
import '../views/common/page_no_connexion.dart';
import '../views/text/text.dart';


final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'root');
final GlobalKey<NavigatorState> _shellNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'shell');

class RouteClass {
  static final supabase = Supabase.instance.client;

  static final router = GoRouter(
    navigatorKey: _rootNavigatorKey,
    initialLocation: supabase.auth.currentUser != null ? "/login" : "/login",
    errorBuilder: (context, state) {
      return PageNoConnected();
    },
    routes: [
      ShellRoute(
        navigatorKey: _shellNavigatorKey,
        builder: (BuildContext context, GoRouterState state, Widget child) {
          return MainPage(child: child);
        },
        routes: <RouteBase>[
          GoRoute(
            path: '/home',
            pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: const HomePage()
            ),
          ),
          GoRoute(path: '/admin',
              pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: const Administration(),),
              ),
          GoRoute(
            path: '/profil',
            pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: const Profil()
            ),
          ),
          //
          GoRoute(
            path: '/historique',
            pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: //PortSerial(),
              Historique()
            ),
          ),
          GoRoute(
            path: '/settings',
            pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: const Settings()
            ),
          ),
          GoRoute(
            path: '/listItems',
            pageBuilder: (context, state) => NoTransitionPage<void>(
                key: state.pageKey,
                child: ListItems()
            ),
          )
        ],
      ),
      GoRoute(
          path: '/login',
          pageBuilder: (context, state) => NoTransitionPage<void>(
            key: state.pageKey,
            restorationId: state.pageKey.value,
            child: const LoginPage(),
          )
      ),
      GoRoute(
          path: '/disconnected',
          pageBuilder: (context, state) => NoTransitionPage<void>(
            key: state.pageKey,
            restorationId: state.pageKey.value,
            child: const PageNoConnected(),
          )
      ),
      GoRoute(
          path: '/scanner-login',
          pageBuilder: (context, state) => NoTransitionPage<void>(
            key: state.pageKey,
            restorationId: state.pageKey.value,
            child: const PageScanner(),
          )
      ),
    ],

  );
}
