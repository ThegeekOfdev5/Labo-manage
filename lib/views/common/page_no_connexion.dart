import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:lottie/lottie.dart';
import 'package:up_pro/controller/controllerDataBase.dart';

class PageNoConnected extends StatefulWidget {
  const PageNoConnected({super.key});

  @override
  State<PageNoConnected> createState() => _PageNoConnectedState();
}

class _PageNoConnectedState extends State<PageNoConnected> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset("assets/lotties/no-connection.json"),
              SizedBox(height: 30,),
              Text("Verifiez votre connexion internet..",style: TextStyle(
                fontSize: 34,color: Colors.grey
              ),),            
            ],
          ),
        ),
      ),
    );
  }
}
