import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../models/etat_pointage.dart';
import '../models/personnel.dart';
import 'package:intl/intl.dart';

import '../views/widgets/showsnackbar.dart';

class ControllerDataBase extends GetxController{
  final supabase = Supabase.instance.client;
  final storage = FlutterSecureStorage();
  var listePersonnel=<PersonnelModel>[].obs;
  var usingListePersonnel=<PersonnelModel>[].obs;
  var listEtat=<EtatModel>[].obs;
  var listPresence=<String>[].obs;
  var listAbsence=<String>[].obs;
  var historyUser=<PersonnelModel>[].obs;
  var historyGlobalUser=<PersonnelModel>[].obs;
  var historyEtat=<EtatModel>[].obs;
  var historyDate="".obs;
  var listNomPrePersonnel=<String>[].obs;
  var personneAdmin=<PersonnelModel>[].obs;
  var listePersonnAdmin=<PersonnelModel>[].obs;
  var noAdminPerson=<PersonnelModel>[].obs;

  Future<void> updatePersonnel(
      {required matricule,required code_empreinte,required nom, required prenom, required contact, required departement, required titre,required email}) async {
    final data = {
      "Matricule": matricule,
      "Nom": nom,
      "Prenom": prenom,
      "Contact": contact,
      "Nom_departement": departement,
      "Nom_titre": titre,
      "Email": email,
      "Code_empreinte":code_empreinte==""?"XXXXXXXX":code_empreinte,
    };
    try {
      await supabase
          .from('Personnel')
          .update(data)
          .eq('Matricule', data["Matricule"])
          .select();
    } on Exception catch (e) {
      print(e.toString());
      throw Exception(e);
    }
  }

  Future<void> deletePersonnel({required matricule}) async {
    try {
      await supabase
          .from('Personnel')
          .delete()
          .eq('Matricule', matricule);
      await supabase
          .from('Etat')
          .delete()
          .eq('matricule', matricule);
    } on Exception catch (e) {
      print("jfeef ${e.toString()}");
      throw Exception(e);

    }
  }
  Future<List<EtatModel>> getAllEtatByItem({required DateTime date}) async {
    final List<dynamic> etat = await supabase.from('Etat').select().eq("times_day", "${date.year}-${date.month}-${date.day}").order("created_at");
    final Etat = etat.map((json) => EtatModel.fromJson(json)).toList();
    return Etat;
  }


  Future<void> addEtat({required matricule}) async {
    final data = {
      "matricule": matricule,
    };
    try {
      await supabase.from('Etat').insert(data).select();
    } on Exception catch (e) {
      throw Exception(e);
    }
  }

  Future<void> addPersonne({required PersonnelModel personne}) async {
    final data = {
      "Matricule": personne.matricule,
      "Nom":personne.nom,
      "Prenom":personne.prenom,
      "Contact":personne.contact,
      "Code_empreinte": personne.code_empreinte==""?"XXXXXXXX":personne.code_empreinte,
      "Nom_departement": personne.departement,
      "Nom_titre": personne.titre,
      "Email": personne.email,
    };
    try {
      await supabase.from('Personnel').insert(data).select();
      await addEtat(matricule: personne.matricule);
    } on Exception catch (e) {
      throw Exception(e);
    }
  }

  Future<List<PersonnelModel>> getAllPersonne() async {
    final List<dynamic> perses = await supabase.from('Personnel').select();
    final List<PersonnelModel>personnes = perses.map((e) =>
        PersonnelModel.fromJson(e)).toList();
    return personnes;
  }
  Stream<List<EtatModel>> getAllEtat()  {
    return supabase
        .from('Etat')
        .stream(primaryKey:["id"])
        .order("created_at")
        .map((listEtat) =>listEtat
        .map((etat) =>EtatModel.fromJson(etat)).toList());

  }

  Future<List<PersonnelModel>> getPersonnesAdmins()async{
    final List<dynamic>emailAdminUsers=await supabase.from('users').select('email');

    final personnes=await getAllPersonne();
    List<PersonnelModel> personnesAdmin=[];
    emailAdminUsers.forEach((email) {
      print("emailAdmin $email");
      personnesAdmin.addAll(personnes.where((personne) =>personne.email==email['email']).toList());
    });

    noAdminPerson.value=usingListePersonnel.where((personne) =>listePersonnAdmin.any((personneAdmin) =>personneAdmin.email!=personne.email) ).toList();

    return personnesAdmin;
  }



  void initialisation()async{
   final personnes=await getAllPersonne();
   listePersonnel.value=personnes;
   usingListePersonnel.value=personnes;
   listePersonnAdmin.value=await getPersonnesAdmins();
    var itemEtats=await getAllEtatByItem(date:DateTime.now());
   // if(itemEtats.isEmpty){
   //   listePersonnel.forEach((personne)async{await addEtat(matricule: personne.matricule);});
   // };
    personnes.forEach((personne) {
      if(itemEtats.where((etat)=>etat.matricule==personne.matricule).isEmpty){
         addEtat(matricule: personne.matricule);
      }
    });
  }


  void staticZone({required List<EtatModel> etats}){
    listAbsence.value=[];
    listPresence.value=[];
    listEtat.value=etats
        .where((etat) =>etat.times.day==DateTime.now().day
        &&etat.times.month==DateTime.now().month
        &&etat.times.year==DateTime.now().year)
        .toList();
    listePersonnel.forEach((personne) {
      List<EtatModel> listePersonnelEtat=listEtat
          .where((etat) =>personne.matricule==etat.matricule)
          .toList();
      if(listePersonnelEtat.first.etat=="Present"){
      listPresence.add("Present");
      }else if(listePersonnelEtat.first.etat=="Absent"){
        listAbsence.add("Absent");
      }
    });
  }

  void HistorySearch({required date})async{
    historyEtat.value=await getAllEtatByItem(date:date);
    historyUser.value=await getAllPersonne();
    if(historyEtat.isEmpty){
      historyUser.clear();
    }else{
      historyUser.forEach((user) {
        final searchUserEtat=historyEtat.any((etat) => etat.matricule==user.matricule);
        if(!searchUserEtat) historyUser.remove(user);
      });
      historyGlobalUser=historyUser;
    }
    listNomPrePersonnel.clear();
    historyGlobalUser.forEach((element) {
      listNomPrePersonnel.add("${element.nom} ${element.prenom}");
    });
  }
  
  void deleteEtatFrequency({frequency,required turnOn})async{
   while (turnOn){
     Future.delayed(Duration(seconds:frequency));
     final times=DateTime.now();
     final day=times.day;
     final month=times.month;
     final year=times.year;
     await supabase.from("Etat").delete().neq("times_day", "${year}-${month}-${day}");
   }
  }
  
  void addAdminUser({required email,context})async{
    try{
      await supabase.auth.signUp(
          email:email,
          password:"Ea${email}77"
      );
     initialisation();
      ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Succes", "Ajout Effectuer avec succes", Colors.green));
    }on Exception catch(e){
      throw "Erreur add: ${e.toString()}";
    }
  }

  void deleteAdminUser({required email})async{
    try{
      await supabase.from('users').delete().eq('email', email);
      listePersonnAdmin.removeWhere((personne) =>personne.email==email);
    }catch(e){
    throw e;
    }
  }

  @override
  void onInit(){
    initialisation();
    super.onInit();
  }

}