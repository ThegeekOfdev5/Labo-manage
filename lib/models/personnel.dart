import 'dart:convert';

class PersonnelModel {
  String matricule;
  String nom;
  String prenom;
  String contact;
  String departement;
  String titre;
  String code_empreinte;
  String email;

  PersonnelModel({
    required this.matricule,
    required this.nom,
    required this.prenom,
    required this.contact,
    required this.departement,
    required this.titre,
    required this.code_empreinte,
    required this.email,
  });

  factory PersonnelModel.fromRawJson(String str) => PersonnelModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PersonnelModel.fromJson(Map<String, dynamic> json) => PersonnelModel(
    nom: json["Nom"],
    matricule: json["Matricule"],
    prenom: json["Prenom"],
    contact: json["Contact"]??"aucun",
    departement: json["Nom_departement"]??"aucun",
    titre: json["Nom_titre"]??"aucun",
      code_empreinte:json["Code_empreinte"]??"aucun",
    email: json["Email"],

  );

  Map<String, dynamic> toJson() => {
    "Matricule": matricule,
    "Nom": nom,
    "Prenom": prenom,
    "Contact": contact,
    "Departement": departement,
    "Titre": titre,
    "Code_empreinte":code_empreinte,
    "Email":email,
  };
}
