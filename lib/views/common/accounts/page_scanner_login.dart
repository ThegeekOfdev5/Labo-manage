import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class PageScanner extends StatefulWidget {
  const PageScanner({super.key});

  @override
  State<PageScanner> createState() => _PageScannerState();
}

class _PageScannerState extends State<PageScanner> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child:Center(
          child: Lottie.asset("assets/lotties/scanner.json"),
        )
      ),
    );
  }
}
