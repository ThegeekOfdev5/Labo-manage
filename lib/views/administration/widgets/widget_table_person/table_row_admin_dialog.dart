import 'dart:math';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:fluid_dialog/fluid_dialog.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';
import 'package:up_pro/views/widgets/showsnackbar.dart';

class RowRowAdminDialog extends StatefulWidget {
  final  PersonnelModel personne;
  final BuildContext PageContext;
  const RowRowAdminDialog({Key? key, required this.personne,required this.PageContext})
      : super(key: key);

  @override
  State<RowRowAdminDialog> createState() => _RowRowAdminDialogState();
}

class _RowRowAdminDialogState extends State<RowRowAdminDialog> {

  bool isHovering = true;
  final supabase = Supabase.instance.client;
  final ScrollController _controller=ScrollController();
  late TextEditingController controllerTextNom;
  late TextEditingController controllerTextPrenom;
  late TextEditingController controllerTextContact;
  late TextEditingController controllerTextTitre;
  late TextEditingController controllerTextDepartement;
  late TextEditingController controllerTextEmail;
  late ControllerDataBase bdController;
  bool switchValue=false;


  @override
  void initState() {
    Get.put(ControllerDataBase());
    bdController=Get.find();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return bdController.listePersonnAdmin.any((personne) =>personne.email==widget.personne.email)?
    SizedBox():InkWell(
      mouseCursor: SystemMouseCursors.basic,
      onTap:(){
        setState(() {
          showDialog(context: context, builder:(context){
            return AlertDialog(
              title:Text("Autorisation"),
              content: Text("Voulez vous ajouter ${widget.personne.nom} ${widget.personne.prenom} comme administrateur"),
              actions: [
                TextButton(onPressed:(){
                  bdController.addAdminUser(email: widget.personne.email,context: context);
                  ScaffoldMessenger.of(widget.PageContext).showSnackBar(showSnackBar("Succes", "Ajout Effectué", Colors.green));
                  Navigator.of(context).pop();
                }, child:Text("Oui")),
                TextButton(onPressed:(){
                  Navigator.of(context).pop();
                }, child: Text("Non"))
              ],
            );
          }
          );
        });
      },
      child: Material(
        elevation:10,
        child: Container(
          padding: const EdgeInsets.only(left: 2.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 1.0),
            color: Color(0xFFF4F4F4),
          ),
          height: 40,
          width: 800,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // Réf
              Container(
                height: 40,
                width: 80,
                alignment: Alignment.centerLeft,
              ),
              // Intitule
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.nom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              //type
              Container(
                height: 40,
                width: 250,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.prenom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),

              //unite
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.contact,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              // Réalise Annuel
              Container(
                  height: 40,
                  width: 160,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.personne.titre,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              // Réalise Mois
              Container(
                  height: 40,
                  width: 150,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.personne.departement,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              //cible
              Container(
                height: 40,
                width: 150,
                margin: EdgeInsets.only(top:5,bottom:5),
                decoration:BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)
                ),
                alignment: Alignment.center,
                child:Text("${widget.personne.code_empreinte}",maxLines: 4,overflow:TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.blueAccent)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}