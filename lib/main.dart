import 'package:connection_notifier/connection_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'route/route.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


void main() async{
  final storage = FlutterSecureStorage();
  await Future.delayed(Duration(seconds: 3));
  await Get.put(ControllerDataBase);
  await Supabase.initialize(
    url: "https://ttrjntfzhfivtebsgeeq.supabase.co",
    anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InR0cmpudGZ6aGZpdnRlYnNnZWVxIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDA3MTA4NjgsImV4cCI6MjAxNjI4Njg2OH0._xcglVrE3Vs-xCM9nRkluX0eN7sfbZKoLFdZCT0ROX0",
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ConnectionNotifier(
      connectionNotificationOptions: const ConnectionNotificationOptions(
        alignment: AlignmentDirectional.bottomCenter,
      ),
    child: MaterialApp.router(
      title: 'LABO-MANAGEMENT',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      routerConfig: RouteClass.router,
      builder: EasyLoading.init(),
    )
    );
  }
}


