import 'package:easy_container/easy_container.dart';
import 'package:flutter/material.dart';

class WidgetSuivieZone extends StatefulWidget {
  final int number;
  final String title;
  final Color? color;
  const WidgetSuivieZone({super.key,required this.number, required this.title, this.color});

  @override
  State<WidgetSuivieZone> createState() => _WidgetSuivieZoneState();
}

class _WidgetSuivieZoneState extends State<WidgetSuivieZone> {
  bool hovered=false;
  @override
  Widget build(BuildContext context) {
    return EasyContainer(
      onHover: (value){setState(() {hovered=!hovered;});},
      height: 80,
      padding: 1,
      onTap:(){},
      color: widget.color,
      borderRadius: 20,
      child:AnimatedCrossFade(
        firstCurve:Curves.ease,
        secondCurve:Curves.ease,
        firstChild: ListTile(
          leading: CircleAvatar(
            radius: 30,
            backgroundColor: Colors.white,
            child: Text("${widget.number}",style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black
            ),),
          ),
          title: Text("${widget.title}",style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: Colors.black
          ),),
        ),
        secondChild: Center(
          child:CircleAvatar(
            radius: 30,
            backgroundColor: Colors.white,
            child: Text("${widget.number}",style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black
            ),),
          ),
        ),
        crossFadeState:hovered?CrossFadeState.showSecond:CrossFadeState.showFirst,
        duration: Duration(milliseconds: 500),
      ),
    );
  }
}
