import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:stepper_a/stepper_a.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/views/administration/widgets/widget_table_person/uid_person.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';

import '../../../../constants/color_global.dart';
import '../../../../models/personnel.dart';
import 'table_gestion_person_row.dart';
import 'table_gestion_persone_entete.dart';
import 'table_row_admin_dialog.dart';


class AddAdminWidget extends StatefulWidget {
  const AddAdminWidget({super.key});

  @override
  State<AddAdminWidget> createState() => _AddAdminWidgetState();
}

class _AddAdminWidgetState extends State<AddAdminWidget> {
  ScrollController scrollController=ScrollController();
  ScrollController _controller=ScrollController();
  late ControllerDataBase bdController;
  final StepperAController controller = StepperAController();
  final supabase = Supabase.instance.client;
  int _currentStep=0;
  bool stepIsActive=false;
  List<PersonnelModel> filtered=[];
  List<dynamic>listUserPerson=[];
  final storage = new FlutterSecureStorage();
  GlobalKey<FormState> formKeyUser=GlobalKey<FormState>();

  @override
  void initState(){
    bdController =Get.find();
    bdController.initialisation();
    filtered=bdController.usingListePersonnel;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(10),
          height:600,
          width:1120,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Ajouter Administrateur",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
              ),
              Container(
                height: 50,
                width: 600,
                child: TextFormField(
                  decoration: InputDecoration(
                      fillColor: Colors.black12,
                      filled: true,
                      hintText: "Rechercher Personne",
                      hintStyle: TextStyle(fontSize: 15),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide:BorderSide(color:colorBlueGlobal)
                      )
                  ),
                  onChanged: (value){
                    setState(() {
                      filtered=bdController.usingListePersonnel.where((personne){
                        final personneNomPrenom="${personne.nom.toLowerCase()} ${personne.prenom.toLowerCase()}";
                        return personne.nom.toLowerCase().contains(value.toLowerCase())||
                            personne.prenom.toLowerCase().contains(value.toLowerCase())||
                            personneNomPrenom.toLowerCase().contains(value.toLowerCase());
                      }
                      ).toList();
                    });
                  },
                ),
              ),
              SizedBox(height:10,),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                child: Container(
                  margin: EdgeInsets.only(top:10),
                  height:400,
                  width: 1200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TableauHeader(),
                      Container(
                        margin: EdgeInsets.only(top:3),
                        height:300,
                        width: 1100,
                        child: VsScrollbar(
                          isAlwaysShown: true,
                          controller: scrollController,
                          child: Obx(()
                          => filtered.length>0?ListView.builder(
                              controller: scrollController,
                              itemCount: filtered.length,
                              itemBuilder:(context,index){
                                return RowRowAdminDialog(personne: filtered[index],PageContext: context,);
                              }):Center(child: Container(
                              height: 60,
                              padding: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(40)
                              ),
                              child: Text("Aucun enregistrment",style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20
                              ),)),
                          ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Positioned(
            top:10,
            right: 20,
            child:IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.close,size:40,color: Colors.black,),
          style: IconButton.styleFrom(
            backgroundColor: Colors.grey.withOpacity(.5)
          ),
        ))
      ],
    );
  }
}

