/*
 * This ESP32 code is created by esp32io.com
 *
 * This ESP32 code is released in the public domain
 *
 * For more detail (instruction and wiring diagram), visit https://esp32io.com/tutorials/esp32-rfid-nfc
 */

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN  10 
#define RST_PIN 5
#define Read_LED 4

MFRC522 rfid(SS_PIN, RST_PIN);


void setup() {
  Serial.begin(115200);
  SPI.begin(); // init SPI bus
  rfid.PCD_Init(); // init MFRC522
  pinMode(Read_LED,OUTPUT);
  rfid.PCD_DumpVersionToSerial();
}

void loop() {
  if (rfid.PICC_IsNewCardPresent()) { // new tag is available
    if (rfid.PICC_ReadCardSerial()) { // NUID has been readed      
      // print UID in Serial Monitor in the hex format
      String uid="";
      for (int i = 0; i < rfid.uid.size; i++) {
        uid=uid+String(rfid.uid.uidByte[i] < 0x10 ? "0" : "");
        uid=uid+String(rfid.uid.uidByte[i], HEX);
      }
      Serial.print(uid);

      digitalWrite(Read_LED,HIGH);
      delay(500);
      digitalWrite(Read_LED,LOW);
      
      rfid.PICC_HaltA(); // halt PICC
      rfid.PCD_StopCrypto1(); // stop encryption on PCD
    }
  }
}


