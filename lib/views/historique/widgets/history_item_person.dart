import 'package:flutter/material.dart';
import '../../../controller/controllerDataBase.dart';
import '../../../models/etat_pointage.dart';
import '../../../models/personnel.dart';

class ItemPersonHistory extends StatefulWidget {
  final PersonnelModel personnel;
  final List<EtatModel> listeEtat;
  const ItemPersonHistory({super.key, required this.personnel, required this.listeEtat });

  @override
  State<ItemPersonHistory> createState() => _ItemPersonHistoryState();
}

class _ItemPersonHistoryState extends State<ItemPersonHistory> {
  ControllerDataBase bdController = ControllerDataBase();
  List<EtatModel> allEtats=[];

  String updateEtat(){

    List<EtatModel> listePersonnelEtat=widget.listeEtat
        .where((etat) =>widget.personnel.matricule==etat.matricule)
        .toList();
    allEtats.clear();
    allEtats.addAll(listePersonnelEtat);

    return listePersonnelEtat.first.etat;
  }

  @override
  Widget build(BuildContext context) {


    return Container(
      height: 43,
      padding: EdgeInsets.all(6),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius:2,
                spreadRadius: 3,
                offset: Offset(2,2),
                color: Colors.grey
            ),
            BoxShadow(
                blurRadius: 4,
                offset: Offset(-2,-2),
                color: Colors.grey
            )
          ]
      ),
      child: Row(
        children: [
          RichText(text: TextSpan(
              text: "Nom :  ",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey
              ),children: [
            TextSpan(
              text:"${widget.personnel.nom}      ",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.black
              ),
            ),
            TextSpan(
                text: "Prenom : ",
                style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey
                )),
            TextSpan(
              text:"${widget.personnel.prenom}        ",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.black
              ),
            ),

          ]
          )),
          SizedBox(width: 10,),
          RichText(text: TextSpan(
              text: "Titre :  ",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey
              ),children: [
            TextSpan(
              text:"${widget.personnel.titre}         ",
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue
              ),
            ),
          ]
          )),
          Spacer(flex: 2,),
          PopupMenuButton(
            tooltip:"Etat",
            child:   Container(
              padding: EdgeInsets.only(left: 7,bottom:3),
              height: 30,
              width: 80,
              decoration: BoxDecoration(
                  color: updateEtat()=="Present"?Colors.green: Colors.red,
                  borderRadius: BorderRadius.circular(10)
              ),
              child:Center(
                child: Text("${updateEtat()}",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: Colors.white,
                ),),
              ),
            ),
            itemBuilder: (BuildContext context) {
              return List.generate(allEtats.length, (index) => PopupMenuItem(child:ListTile(
                leading:Text("${allEtats[index].times.day}/${allEtats[index].times.month}/${allEtats[index].times.year}:${allEtats[index].times.hour}h${allEtats[index].times.minute}"),
                title:   Container(
                    padding: EdgeInsets.only(left: 7,bottom:3),
                    height: 30,
                    width: 80,
                    decoration: BoxDecoration(
                        color: allEtats[index].etat=="Present"?Colors.green:Colors.red,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child:Center(
                      child: Text("${allEtats[index].etat}",style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Colors.white,
                      ),),
                    )),
              )));
            },)
        ],
      ),
    );
  }

}

