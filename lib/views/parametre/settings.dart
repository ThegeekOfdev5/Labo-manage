import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hsvcolor_picker/flutter_hsvcolor_picker.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/views/widgets/showsnackbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants/color_global.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final _colorNotifier = ValueNotifier<Color>(Colors.green);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10, left: 15, bottom: 10, right: 10),
        padding: EdgeInsets.all(15),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                surfaceTintColor: Colors.white,
                elevation: 10,
                child: ListTile(
                  title: Text("Rénitialisation de l'historique",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Colors.black)),
                  trailing: Icon(Icons.online_prediction),
                  onTap: () async {
                    setState(() async {
                      final Uri _url = Uri.parse(
                          'https://supabase.com/dashboard/project/ttrjntfzhfivtebsgeeq/editor/29597');
                      if (!await launchUrl(_url)) {
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar(
                            "Error", "Could not launch $_url", Colors.red));
                      }
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                surfaceTintColor: Colors.white,
                elevation: 10,
                child: ListTile(
                  leading: TextButton(
                    child: Text("Personnaliser couleur",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Colors.black)),
                    onPressed: () {
                      setState(() {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                shape:RoundedRectangleBorder(),
                                children: [
                                  Container(
                                    height:350,
                                    width:400,
                                    padding: EdgeInsets.symmetric(horizontal:12),
                                    child: ValueListenableBuilder<Color>(
                                      valueListenable: _colorNotifier,
                                      builder: (_, color, __) {
                                        return ColorPicker(
                                          color: color,
                                          onChanged: (value) {
                                            setState(() {
                                              colorBlueGlobal = value;
                                            });
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              );
                            });
                      });
                    },
                  ),
                  title: Center(
                    child:
                    Text("Couleur : $colorBlueGlobal",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Colors.black)),
                  ),
                  trailing: IconButton(
                      tooltip: 'Couleur par Defaut',
                      onPressed: () {
                        setState(() {
                          colorBlueGlobal = Color(0xFF201658);
                        });
                      },
                      icon: Icon(Icons.change_circle_outlined)),
                ),
              ),
            ),
            Expanded(child: Center(
              child: Lottie.asset("assets/lotties/search_setting.json"),
            )),
          ],
        ));
  }
}
