import 'package:flutter/material.dart';
import 'package:flutter_toggle_tab/flutter_toggle_tab.dart';
import 'package:flutter_toggle_tab/helper.dart';
import 'package:get/get.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/views/administration/widgets/section_gestion_administration.dart';
import 'package:up_pro/views/administration/widgets/section_persons.dart';

import '../../constants/color_global.dart';
import '../list_items/list_items.dart';

class Administration extends StatefulWidget {
  const Administration({super.key});

  @override
  State<Administration> createState() => _AdministrationState();
}

class _AdministrationState extends State<Administration> with SingleTickerProviderStateMixin{

  final ValueNotifier<int> _tabIndexBasicToggle = ValueNotifier(1);
  List<String> get _listTextTabToggle =>["Listes du personnel","Liste des administrateurs"];
  List<Widget> divider() => [SizedBox(height: heightInPercent(2, context)), const Divider(thickness: 2), SizedBox(height: heightInPercent(2, context)),];
  List<Widget> widgets=[ListePersonnel(),GestionAdministration()];

  late ControllerDataBase DBcontroller;

  @override
  void initState() {
   DBcontroller=Get.find();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top:7, left: 15, bottom: 10, right: 10),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: SingleChildScrollView(
            padding: EdgeInsets.only(top:36,left:10,right: 10),
            child: basicTabToggle()
        ));
  }
  Widget basicTabToggle() =>
      Column(
        children: [
          Center(
            child: ValueListenableBuilder(
              valueListenable: _tabIndexBasicToggle,
              builder: (context, currentIndex, _) {
                return FlutterToggleTab(
                  // width in percent
                  width: 70,
                  borderRadius: 30,
                  height: 50,
                  selectedIndex: currentIndex,
                  selectedBackgroundColors: [
                    colorBlueGlobal,
                    colorBlueGlobal,
                  ],
                  selectedTextStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                  unSelectedTextStyle: const TextStyle(
                    color: Colors.black87,
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                  labels: _listTextTabToggle,
                  selectedLabelIndex: (index) {
                   setState(() {
                     _tabIndexBasicToggle.value = index;
                     DBcontroller.initialisation();
                   });
                  },
                  isScroll: false,
                );
              },
            ),
          ),
          SizedBox(height: 30,),
          ValueListenableBuilder(
            valueListenable: _tabIndexBasicToggle,
            builder: (context, currentIndex, _) {
              return widgets[currentIndex];
            },
          ),
        ],
      );
}
