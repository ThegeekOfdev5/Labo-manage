
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';
import 'package:up_pro/views/list_items/list_items.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../widgets/showsnackbar.dart';


class RowTableGestionPersonAdmin extends StatefulWidget {
  final  PersonnelModel personne;
  final BuildContext pageContext;
  const RowTableGestionPersonAdmin({Key? key, required this.personne, required this.pageContext})
      : super(key: key);

  @override
  State<RowTableGestionPersonAdmin> createState() => _RowTableGestionPersonAdminState();
}

class _RowTableGestionPersonAdminState extends State<RowTableGestionPersonAdmin> {

  bool isHovering = true;
  final supabase = Supabase.instance.client;
  late TextEditingController controllerTextNom;
  late TextEditingController controllerTextPrenom;
  late TextEditingController controllerTextContact;
  late TextEditingController controllerTextTitre;
  late TextEditingController controllerTextDepartement;
  late TextEditingController controllerTextEmail;
  late ControllerDataBase bdController;
  bool switchValue=false;

  void updateModifedCell()async{
    controllerTextNom.text=widget.personne.nom;
    controllerTextPrenom.text=widget.personne.prenom;
    controllerTextContact.text=widget.personne.contact;
    controllerTextTitre.text=widget.personne.titre;
    controllerTextDepartement.text=widget.personne.departement;
    controllerTextEmail.text=widget.personne.email;
  }
  @override
  void initState() {
    Get.put(ControllerDataBase());
    bdController=Get.find();
    controllerTextNom=TextEditingController();
    controllerTextPrenom=TextEditingController();
    controllerTextContact=TextEditingController();
    controllerTextTitre=TextEditingController();
    controllerTextDepartement=TextEditingController();
    controllerTextEmail=TextEditingController();
    updateModifedCell();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      mouseCursor: SystemMouseCursors.basic,
      onHover: (value) {
        setState(() {
          isHovering = value;
        });
      },
      child: Material(
        elevation:10,
        child: Container(
          padding: const EdgeInsets.only(left: 2.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 1.0),
            color: Color(0xFFF4F4F4),
          ),
          height: 40,
          width: 800,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // Réf
              Container(
                height: 40,
                width: 80,
                alignment: Alignment.centerLeft,
                color: Colors.transparent,
                child: Row(
                  children: [
                    bdController.personneAdmin.first.email==widget.personne.email?
                    IconButton(
                      tooltip: "Administrateur",
                      onPressed:null,
                      splashRadius:5,
                      icon: Icon(Icons.person, size: 20,
                        color: Colors.red,),
                    )
                        :IconButton(
                      tooltip: "Supprimer droit administrateur",
                      onPressed:()async{
                        await delete();
                      },
                      splashRadius:5,
                      icon: Icon(Icons.delete_forever_outlined,size: 20,
                        color: Colors.red,),
                    ),
                  ],
                ),
              ),
              // Intitule
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.nom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              //type
              Container(
                height: 40,
                width: 250,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.prenom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),

              //unite
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.contact,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              // Réalise Annuel
              Container(
                  height: 40,
                  width: 160,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.personne.titre,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              // Réalise Mois
              Container(
                  height: 40,
                  width: 150,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.personne.departement,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              //cible
              Container(
                height: 40,
                width: 150,
                margin: EdgeInsets.only(top:5,bottom:5),
                decoration:BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)
                ),
                alignment: Alignment.center,
                child:Text(widget.personne.code_empreinte,maxLines: 4,overflow:TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.blueAccent)),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future<void> delete() async {
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title:Text("Suppresion de personnes"),
            contentPadding:EdgeInsets.all(10),
            content:Container(padding: EdgeInsets.all(10),height:90,width:300,child: Center(child: Text("Voulez-vous vraiment enlever le droit administrateur?",textAlign: TextAlign.center,style: TextStyle(fontSize: 20,color: Colors.black),))),
            actions: [
              OutlinedButton(
                  onPressed:(){
                    setState(()async{
                      try{
                        bdController.listePersonnAdmin.removeWhere((personne) =>personne.email==widget.personne.email);
                        bdController.deleteAdminUser(
                            email: controllerTextEmail.text,
                           );

                        ScaffoldMessenger.of(widget.pageContext).showSnackBar(showSnackBar("Succès","Suppresion effectuée avec succès\nVeuillé actualiser",Colors.green));
                        Navigator.of(context).pop();
                      }on Exception catch(e){
                        ScaffoldMessenger.of(widget.pageContext).showSnackBar(showSnackBar("Erreur",e.toString(),Colors.red));
                      }
                    });
                  },
                  child:Text("Oui")
              ),
              SizedBox(width: 100,),
              OutlinedButton(
                  onPressed:(){
                    Navigator.of(context).pop();
                  },
                  child:Text("Non")
              ),
            ],
          );
        });
  }
}