import 'package:flutter/material.dart';
import 'package:get/get_utils/src/get_utils/get_utils.dart';
import 'package:stepper_a/stepper_a.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';

import '../../../widgets/showsnackbar.dart';

class FormulaireAddUser extends StatefulWidget {
  final BuildContext pageContext;
  const FormulaireAddUser({
    Key? key, required this.pageContext,
  }) : super(key: key);

  @override
  State<FormulaireAddUser> createState() => _FormulaireAddUserState();
}

class _FormulaireAddUserState extends State<FormulaireAddUser> {
  bool _obsureText = true;
  String? entity;
  String? acces;
  bool _obsureTextConfirm = true;
  final GlobalKey<FormState> _formAddUser = GlobalKey<FormState>();
  TextEditingController passWordController = TextEditingController();
  final data = <String, dynamic>{};
  bool enableButton = true;
  bool? formResult;
  String resultMessage = "";
  String passwordEmail = "";

  final TextEditingController _matricule = TextEditingController();
  final TextEditingController _nom = TextEditingController();
  final TextEditingController _prenom = TextEditingController();
  final TextEditingController _contact = TextEditingController();
  final TextEditingController _titre = TextEditingController();
  final TextEditingController _departement = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final ControllerDataBase dbController = ControllerDataBase();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width:300,
      child: Form(
            autovalidateMode: AutovalidateMode.disabled,
            key: _formAddUser,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'Matricule ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 60,
                  child: TextFormField(
                    controller: _matricule,
                    onChanged: (value) {
                      setState(() {
                      });
                    },
                    validator: (value) => (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding:
                        const EdgeInsets.only(left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Nom ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _nom,
                    onChanged: (value) {
                      setState(() {

                      });
                    },
                    validator: (value) => (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding:
                        const EdgeInsets.only(left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Prenom ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _prenom,
                    onChanged: (value) {
                      setState(() {

                      });
                    },
                    validator: (value) => (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding:
                        const EdgeInsets.only(left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Contact ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _contact,
                    onChanged: (value) {
                      setState(() {

                      });
                    },
                    validator: (value) =>
                    (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding: const EdgeInsets.only(
                            left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(height: 5,),
                RichText(
                  text: TextSpan(
                    text: 'Departement ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _departement,
                    onChanged: (value) {
                      setState(() {

                      });
                    },
                    validator: (value) =>
                    (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding: const EdgeInsets.only(
                            left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .primaryColor,
                                width: 2))),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Email ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _email,
                    onChanged: (value) {
                      setState(() {

                      });
                    },
                    validator: (value) =>
                    (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding: const EdgeInsets.only(
                            left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(height:5,),
                SizedBox(height:5,),
                RichText(
                  text: TextSpan(
                    text: 'Titre ',
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: '*',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: 300,
                  height: 45,
                  child: TextFormField(
                    controller: _titre,
                    onChanged: (value) {
                      setState(() {
                      });
                    },
                    validator: (value) =>
                    (value != null && value.length > 2)
                        ? null
                        : 'Erreur de saisie',
                    decoration: InputDecoration(
                        hintText: "",
                        contentPadding: const EdgeInsets.only(
                            left: 10.0, right: 20.0),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .primaryColor,
                                width: 2))),
                  ),
                ),
                SizedBox(height:10,),
                ElevatedButton(
                    onPressed:(){
                  setState(() {
                  if(_formAddUser.currentState!.validate()){
                    Navigator.of(context).pop;
                    try{
                      dbController.addPersonne(personne: PersonnelModel(
                          matricule:_matricule.text,
                          nom: _nom.text,
                          prenom: _prenom.text,
                          contact: _contact.text,
                          departement: _departement.text,
                          titre:_titre.text,
                          code_empreinte:"",
                          email: _email.text,

                      )
                      );
                      ScaffoldMessenger.of(widget.pageContext).showSnackBar(showSnackBar("Succès","Personne Ajoutée avec succès\nVeuillez actualiser",Colors.green));

                    }on Exception catch(e){
                      ScaffoldMessenger.of(widget.pageContext).showSnackBar(showSnackBar("Erreur","${e.toString()}",Colors.red));
                    }
                    }
                  });
                },style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.yellow,
                  shape: RoundedRectangleBorder(),
                  minimumSize: Size(300,50),
                  elevation:7
                )
                , child:Text("Ajouter Utilisateur",style: TextStyle(fontSize: 20,color:Colors.white),))
              ],
            ),//gv
          ),
    );
  }
}
