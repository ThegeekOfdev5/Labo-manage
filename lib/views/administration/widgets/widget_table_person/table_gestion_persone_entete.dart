import 'package:flutter/material.dart';

class TableauHeader extends StatefulWidget {
  const TableauHeader({Key? key}) : super(key: key);

  @override
  State<TableauHeader> createState() => _TableauHeaderState();
}

class _TableauHeaderState extends State<TableauHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left:5,right: 5),
      height: 50,
      width: 1100,
      decoration: BoxDecoration(
            color: Colors.brown,
            borderRadius: BorderRadius.only(topRight:Radius.circular(20),topLeft:Radius.circular(20))
        ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
          children: [
            DashBoardHeaderTitle(color: Colors.transparent, size:70,title: "",),
            DashBoardHeaderTitle(color: Colors.transparent, size:150,title: "Nom",),
            DashBoardHeaderTitle(color: Colors.transparent, size:250,title: "Prenom",),
            DashBoardHeaderTitle(color: Colors.transparent, size:150,title: "Contact",),
            DashBoardHeaderTitle(color: Colors.transparent, size: 150,title: "Titre",),
            DashBoardHeaderTitle(color: Colors.transparent, size: 150,title: "Departement",),
            DashBoardHeaderTitle(color: Colors.transparent, size: 150,title: "Code d'empreinte",),
          ],
        ),
    );
  }
}

class DashBoardHeaderTitle extends StatefulWidget {
  final double size;
  final Color color;
  final String title;
  const DashBoardHeaderTitle({Key? key, required this.size, required this.color, required this.title}) : super(key: key);

  @override
  State<DashBoardHeaderTitle> createState() => _DashBoardHeaderTitleState();
}

class _DashBoardHeaderTitleState extends State<DashBoardHeaderTitle> {
  bool isHovering = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){},
      onHover: (value){
        setState(() {
          isHovering = value;
        });
      },
      child: Container(
        width: widget.size,
        color: isHovering ? Color(0xFF8B735C) :widget.color,
        alignment: Alignment.centerLeft,
        child: Text("${widget.title}",style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.bold),),
      ),
    );
  }
}


class IndicateurTitle extends StatefulWidget {
  final double size;
  final Color color;
  final String title;
  const IndicateurTitle({Key? key, required this.size, required this.color, required this.title}) : super(key: key);

  @override
  State<IndicateurTitle> createState() => _IndicateurTitleState();
}

class _IndicateurTitleState extends State<IndicateurTitle> {
  bool isHovering = false;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: (){},
        onHover: (value){
          setState(() {
            isHovering = value;
          });
        },
        child: Container(
          width:double.infinity,
          padding: const EdgeInsets.only(left: 2.0),
          color: isHovering ? Color(0xFF8B735C) :widget.color,
          alignment: Alignment.centerLeft,
          child: Text("${widget.title}",style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.bold),),
        ),
      ),
    );
  }
}

