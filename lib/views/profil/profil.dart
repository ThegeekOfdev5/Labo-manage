import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_slimy_card/flutter_slimy_card.dart';
import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';

import '../../constants/color_global.dart';
import '../widgets/showsnackbar.dart';


class Profil extends StatefulWidget {
  const Profil({super.key});

  @override
  State<Profil> createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  final supabase = Supabase.instance.client;
  final storage=FlutterSecureStorage();
  final TextEditingController _passwordController=TextEditingController();
  late ControllerDataBase dbController;
  GlobalKey<FormState> _formKey=GlobalKey<FormState>();
  final RegExp regex = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$');
  bool isChanging=false;

  @override
  void initState() {
    dbController=Get.find();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
        margin: EdgeInsets.only(top: 10, left: 15, bottom: 10, right: 10),
        padding: EdgeInsets.all(20),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child:Column(
          children: [
            SizedBox(height:30,),
            FlutterSlimyCard(
              color: colorBlueGlobal,
              bottomCardHeight: 200,
              topCardHeight:200,
              cardWidth:400,
              topCardWidget: topWidget(),
              bottomCardWidget: bottomWidget(),
            )
          ],
        )
    );
  }
  Widget topWidget(){
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(child: CircleAvatar(radius:40,backgroundColor: Colors.white,child:Icon(Icons.person,size:70,),),),
          Center(child: Text('${dbController.personneAdmin.first.email}',style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold,color: Colors.white))),
        ],
      ),
    );
  }
  Widget bottomWidget() {

    return Container(
      height:200,
      margin: EdgeInsets.only(top:1),
      padding: EdgeInsets.all(2),
      child:isChanging?Center(child: Container(
        height:50,
        width: 50,
        child: CircularProgressIndicator(

        ),
      ),):Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              height: 70,
              width: double.infinity,
              child:TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    hintText: "Password..",
                    prefixIcon: Icon(Icons.security_sharp),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    )
                ),
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      !regex.hasMatch(value)) {
                    return 'Le mot de passe doit avoir au moins de 8 caractères.';
                  }
                  return null;
                },
              ),
            ),
            SizedBox(height: 5,),
            MaterialButton(
              height:60,
              minWidth: 250,
              onPressed:(){
                setState(() {
                  if(_formKey.currentState!.validate()){
                    try{
                      isChanging=true;
                      supabase.auth.updateUser(
                        UserAttributes(
                            email:supabase.auth.currentUser?.email,
                            password:_passwordController.text
                        ),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Succes","Enregistrement fait avec succes",Colors.green));
                      Future.delayed(Duration(seconds:1));
                      isChanging=false;
                      _passwordController.text="";
                    }catch(e){
                      ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Erreur","${e.toString()}",Colors.red));
                    }
                  }
                });
              },color: Colors.orange,
              child: Text("Enregistrer",style: TextStyle(fontSize: 18,color: Colors.white),),
            )
          ],
        ),
      ),
    );
  }
}
